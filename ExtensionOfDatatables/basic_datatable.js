﻿/**
 * Class allows user to perform
 * - filtering
 * - delition of data
 * - ability to select individual or all items in the table
 * - ability to store selected items
 * - limit amount of items that user can select
 */
var BasicDataTable = function (data) {
    this.vars = {
        selectedItems: [],
        autoHideCheckBoxes: false, // used to hide checkbox when delete button on the row is missing
    };

    var root = this;


    this.construct = function (data) {
        root.vars.bulkDeleteMethod = "POST";
        $.extend(root.vars, data);
        root.vars.myData = {};
        declareDataTables();

		// if bulk delete was enabled then run commands below..
        if (typeof root.vars.disableBulkDelete === 'undefined' || root.vars.disableBulkDelete !== false) {
            toggleButtons();
            bulkDelete();
            performBulkDelete();
            openDeleteModal();
        }

		// if filter can be cleared then...
        if (typeof root.vars.applyFilterClear === 'undefined') {
        } else if (root.vars.applyFilterClear !== false) {
            filterCanBeReset();
        }


        toggleSingleBox();
        bindClearSelectedItems(); 
    }
	/**
	 * Method initiates datatables
	 */
    var declareDataTables = function () {
        var tableDefinition = getTableDefinitions();

        root.vars.table.DataTable(tableDefinition);
    };
    /**
     * regular definitions used for datatables
     * @returns {BasicDataTable.getTableDefinitions.basicDatatablesAnonym$2|BasicDataTable.getTableDefinitions.basicDatatablesAnonym$0}
     */
    var getTableDefinitions = function () {
        var self = this;

        if (typeof root.vars.disableBulkDelete === 'undefined') {
            return {
                stateSave: true,
                language: {
                    url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/" + root.vars.locale + ".json"
                },
                processing: true,
                serverSide: true,
                ajax: {
                    'url': root.vars.ajaxUrl,
                    "data": function (d) {
                        return  $.extend(d, root.vars.myData);
                    }
                },
                "autoWidth": false,
                columns: root.vars.columnsObj,
                'columnDefs': [{
                        'targets': 0,
                        'searchable': false,
                        'orderable': false,
                        'className': 'dt-body-center',
                        'render': function (data, type, full, meta) {
                            // why was this statement commented out?
                            // this is because some items might still have usages i.e. for reports
                            //if(full.action.search("button-delete") !== -1) {
                            if(root.vars.autoHideCheckBoxes == false || full.action.search("button-delete") !== -1) {
                                // normal behaviour
                                return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
                            }
                            
                            return '';
                            //}
                            //return '';
                        }
                    }],
                'order': root.vars.columnsOrder,
                "drawCallback": function (settings) {
                    if (root.vars.selectedItems.length < 1) {
                        return;
                    }


                    for (var i = 0; i < root.vars.selectedItems.length; i++) {
                        var ele = root.vars.table.find('input:checkbox[value="' + root.vars.selectedItems[i] + '"]');

                        if (ele.length > 0) {
                            ele.prop('checked', true);
                        }
                    }


                    checkMaxSelectedItemsAllowance();
                }
            };
        } else {
            return {
                stateSave: true,
                language: {
                    url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/" + root.vars.locale + ".json"
                },
                processing: true,
                "autoWidth": false,
                serverSide: true,
                ajax: {
                    'url': root.vars.ajaxUrl,
                    "data": function (d) {
                        return  $.extend(d, root.vars.myData);
                    }
                },
                columns: root.vars.columnsObj,
                'order': root.vars.columnsOrder,
                fnDrawCallback: function () {
                    preSelectItems();
                }
            };
        }
    };
    /**
     * Method enables/disables clear filter button
     * @returns {undefined}
     */
    var filterCanBeReset = function () {
        var currentFilter = root.vars.table.dataTable().api().columns().search();
        var filter_can_be_reset = currentFilter.filter(function (val) {
            return val === ""
        }).length != currentFilter.length;

        if (filter_can_be_reset || root.vars.table.dataTable().api().search().length > 0) {
            // enable filter clear button
            $('.clear_filter_button').show();
        } else {
            // disable filter clear button
            $('.clear_filter_button').hide();
        }
    };
    /**
     * Method adds onclick event to perform ?
     * @returns {undefined}
     */
    var bulkDelete = function () {
        // Bulk Delete
        // Handle click on "Select all" control
        $('body').on('click', root.vars.tableIdString + ' #select-all', function () {
            toggleSelectAll(this.checked);
        });
    };

    /**
     * Method toggles various buttons related to data tables
     * @returns {undefined}
     */
    var toggleButtons = function () {
        var countCheckedCheckboxes = root.vars.selectedItems.length;
        if (countCheckedCheckboxes > 0) {

            if (typeof root.vars.disableBulkDelete === 'undefined') {
                $(root.vars.deleteButtonId).show();
            }
            $('.basicDatatablesSelectableButtons').show();
            $('.clear_selected_items').show();
            if (typeof root.vars.shareReportButtonId !== 'undefined') {
                $(root.vars.shareReportButtonId).show();
            }
        } else {

            if (typeof root.vars.disableBulkDelete === 'undefined') {
                $(root.vars.deleteButtonId).hide();
            }
            $('.basicDatatablesSelectableButtons').hide();
            $('.clear_selected_items').hide();
            if (typeof root.vars.shareReportButtonId !== 'undefined') {
                $(root.vars.shareReportButtonId).hide();
            }
        }
    };

    /**
     * Method preselects items in the row - if those are selected
     * @returns {undefined}
     */
    var preSelectItems = function () { 
        root.vars.table.find('input[type="checkbox"][name="id[]"]').each(function () {
            var indexOf = root.vars.selectedItems.indexOf($(this).val());
             
            if (indexOf !== -1) {
                $(this).prop('checked', true);
            }
        });
        
        // we need to disable check boxes if needed
        checkMaxSelectedItemsAllowance();
    };

    /**
     * Bind clear select items to the modal
     * @returns {undefined}
     */
    var bindClearSelectedItems = function () {
        $('body').on('click', '.clear_selected_items', function () {
            root.vars.selectedItems = [];
            checkMaxSelectedItemsAllowance();
            root.vars.table.find('input[type="checkbox"]').prop('checked', false);
            toggleButtons();
        });
    };

    /**
     * Method is triggered either manully or due to onclick even defined in method "bulkDelete"
     * used to store select items in the array
     * 
     * @param {type} checked
     * @returns {undefined}
     */
    var toggleSelectAll = function (checked) {
        // Check/uncheck all checkboxes in the table
        var rows = root.vars.table.DataTable().rows({'search': 'applied'}).nodes();
        //vars.table.find('input[type="checkbox"]', rows).each.prop('checked', checked);

        root.vars.table.find('input[type="checkbox"]', rows).each(function () {
            $(this).prop('checked', checked);
            var fileId = $(this).val();
            console.log(fileId);
            if (checked) {
                var index = root.vars.selectedItems.indexOf(fileId);

                if (index < 0) {
                    root.vars.selectedItems.push(fileId);
                }
            } else {
                var index = root.vars.selectedItems.indexOf(fileId);

                if (index > -1) {
                    root.vars.selectedItems.splice(index, 1);
                }
            }
            console.log(root.vars.selectedItems);
        });

        toggleButtons();
    };

    /**
     * Select/Diselect a checkbox
     * @returns {undefined}
     */
    var toggleSingleBox = function () {
        // Handle click on checkbox to set state of "Select all" control 
        $('body').on('change', root.vars.tableIdString + ' tbody input[type="checkbox"][name="id[]"]', function () {
            var fileId = $(this).val();
            var index = root.vars.selectedItems.indexOf(fileId);
            // If checkbox is not checked
            if (!this.checked) {
                var el = $(root.vars.tableIdString + ' #select-all').get(0);
                // If "Select all" control is checked and has 'indeterminate' property
                if (el && el.checked && ('indeterminate' in el)) {
                    // Set visual state of "Select all" control 
                    // as 'indeterminate'
                    el.indeterminate = true;
                }

                // remove items from list of selected Items

                if (index > -1) {
                    root.vars.selectedItems.splice(index, 1);
                }
            } else {
                if (index < 0) {
                    root.vars.selectedItems.push(fileId);
                }
            }

            checkMaxSelectedItemsAllowance();

            toggleButtons();
        });
    };

    /**
     * Method used to check if maximum select was reached
     * @returns {undefined}
     */
    var checkMaxSelectedItemsAllowance = function () {
        if (typeof root.vars.maxSelectedItems != "undefined") {
            if (root.vars.selectedItems.length >= root.vars.maxSelectedItems) {
                $(root.vars.tableIdString + ' tbody td:first-child input[type="checkbox"][name="id[]"]:not(:checked)').prop('disabled', true);
            } else {
                $(root.vars.tableIdString + ' tbody td:first-child input[type="checkbox"][name="id[]"]').prop('disabled', false);
            }
        }
    };

    /**
     * Method performs bulk delete
     * @returns {undefined}
     */
    var performBulkDelete = function () { 
        if(typeof root.vars.deleteButtonId == "undefined" || root.vars.deleteButtonId == false || root.vars.deleteButtonId == null || root.vars.deleteButtonId.length < 1) {
            return;
        }
        
        // Handle form submission event
        $('body').on('click', root.vars.deleteButtonId, function (e) {
            e.preventDefault();
            if (!confirm(Lang.get('common.are_you_sure'))) {
                return false;
            }


            var form = root.vars.table.parents('form');
            // Iterate over all checkboxes in the table

            for (var i = 0; i < root.vars.selectedItems.length; i++) {
                var ele = root.vars.table.find('input[value="' + root.vars.selectedItems[i] + '"]');

                if (ele.length < 1) {
                    $(form).append(
                            $('<input>')
                            .attr('type', 'hidden')
                            .attr('name', 'id[]')
                            .val(root.vars.selectedItems[i])
                            );
                }
            }

            $.ajax({
                url: root.vars.bulkDeleteURL,
                data: form.serialize(),
                dataType: 'json',
                method: root.vars.bulkDeleteMethod,
                success: function (data) {
                    miniToastr.success(data['message']);
                    root.vars.selectedItems = [];
                    root.reload();

                    $(root.vars.deleteButtonId).hide();
                    $('.basicDatatablesSelectableButtons').hide();
                    $('#select-all').prop('checked', false);

                },
                error: function (e) {
                    console.log(e);
                    miniToastr.error(Lang.get('common.check_internet_connection'));
                }
            });
        });
    };
	/**
	 * Method triggers delete modal
	 */
    var openDeleteModal = function () {
        // Single Button Click
        $('body').on('click', root.vars.tableIdString + ' .deleteItem', function () {
            $('#deleteModal').modal('show');
            //var activeTable = $(this).parents('.table').attr('id');
            $('#deleteModal form').attr('data-active-table', root.vars.tableIdString);
            $('#deleteModal #entry_id').val($(this).attr('data-entry-id'));
            $('#deleteModal form').attr('action', $(this).attr('data-action'));
        });
    };
	/**
	 * Method can be used to update ajax data within datatable
	 */
    this.setAjaxData = function (data) {
        root.vars.myData = data;
    };
	/**
	 * Method used to reload existing data in the datatable
	 */
    this.reload = function () {
        root.vars.table.DataTable().ajax.reload(null, false);
    };
	/**
	 * Method used to reset filter
	 */
    this.resetFilter = function () {
        root.vars.table.dataTable()
                .api()
                .search('')
                .columns().search('')
                .draw();
    }


    this.construct(data);
};