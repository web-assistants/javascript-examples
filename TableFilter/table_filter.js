﻿/**
 * Used to perform filtering of data in the table
 * works with datatables, jQuery & mini toastr
 */
var tableFilters = function () {
    var vars = {

    };

    var root = this;

    this.construct = function (data) { 
        enableUseFilter();
        enableResetFilter();
        enableApplyFilter();
    };
	
	/**
	 * Add use filter trigger
	 */
    var enableUseFilter = function () { 
        $('body').on('click', '.useFilter', function () {
            resetSearch();

            var value = JSON.parse($(this).attr('data-value'));

            performTableFilter(value);

            $('#loadFilter').modal('hide');
        });
    };

	/**
	 * Add reset search trigger
	 */
    var enableResetFilter = function () { 
        $('body').on('click', '.resetSearch', function () {
            resetSearch();
        });
    };

    /**
	 * Add apply search trigger
	 */
    var enableApplyFilter = function () { 
        $('body').on('click', '.applySearch', function () {
            var data = $('#newFilter form').serializeArray(); 
            performTableFilter(data); 
        });
    };
    
    /**
	 * Perform table filter
	 */
    var performTableFilter = function (value) { 
		// reset current search
        resetSearch(false);
		
		// loop through the values
        var i = 0;
        for (var key in value) {
            var filterValue = value[key]; 
			// preset parameter that need to be skipped
            if (filterValue['name'] == "page_id" || filterValue['name'] == "name") {
                continue;
            }

            if (filterValue['value'].length > 0) {
				// if value is set then we can perform filter on the table
                i++; 
                table.dataTable().fnFilter(filterValue['value'], rowsArr[filterValue['name']]);
            }
        }

		// if no filtering took place -> reset search
        if (i === 0) {
            // reset filter
            resetSearch(false);
        } else {
			// display notification
            miniToastr.success(Lang.get('table_filter.filter_was_applied'));
        }
        
        
        // toggle filter clear button
        root.filter_can_be_reset();
    };
    
	/**
	 * Method toggles filter reset button
	 * Reset button is displayed if filtering was applied
	 */
    this.filter_can_be_reset = function() {
        var currentFilter = table.dataTable().api().columns().search();
        var filter_can_be_reset = currentFilter.filter(function(val){ return val === "" }).length != currentFilter.length;
        
         
        if (filter_can_be_reset || table.dataTable().api().search().length > 0) {
            // enable filter clear button
            $('.clear_filter_button').show();
        }else{
            // disable filter clear button
            $('.clear_filter_button').hide();
        }
    };

    var resetSearch = function(notify = true) { 
        table.dataTable()
                .api()
                .search('')
                .columns().search('')
                .draw();
        
        if (notify === true) {
            miniToastr.success(Lang.get('table_filter.filter_was_cleared'));
        }
        
        // toggle filter clear button
        root.filter_can_be_reset();
    };

    this.construct();
};