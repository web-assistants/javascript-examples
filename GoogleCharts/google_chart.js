google.load('visualization', '1.0', {'packages': ['charteditor', 'controls']});
/**
 * Class extends google charts with following features
 * - ability to show/hide rows
 * - ability to show/hide columns
 * - ability to reset zoom
 * - ability to replace data that is shown on the screen via AJAX
 * - ability to show next/prev set of data IF the data is too large to display on a single screen
 * - ability to show data in a bar chart, line chart or table
 * - ability to preapply/apply templates. template contain a set of columns to display.
 * - Ability to reset the zoom
 * - Ability to cut data from the graph 
 * - Ability to select points on the graph to perform further actions with data in question
 *
 * jQuery & Google Charts
 * Class requires refactoring
 */

//google.setOnLoadCallback(drawDashboard);
var GChart = GChart || (function () {
    var _graphType;
    var _minTime;
    var _maxTime;
    var _hAxisTitle;
    var _vAxisTitle;
    var _tableData;
    var _data;
    var _original_data;
    var _dashboard;
    var _chart;
    var _button;
    var _showChart;
    var _csv_out;
    var _columns;
    //var _series;
    var _view;
    var _selectedColumns;
    var _toggledColumns;
    var _columnNamesWithId;
    var _lineChartRangeFilter;
    var _rows;
    var _toggledRows;
    var _selectedRows;
    var _chartEditor;
    var _initialySelectedColumns = null;
    var _initiallySelectedRows = null;

    var startpoint;
    var endpoint;

    var excludeflag = true;

    return {

        init: function (tableData, graphType, minTime, maxTime, hAxisTitle, vAxisTitle) {
            // google charts
            //_google = google;
            _tableData = tableData;

            // load chart params 
            _graphType = graphType;
            _minTime = minTime;
            _maxTime = maxTime;
            _hAxisTitle = hAxisTitle;
            _vAxisTitle = vAxisTitle;

            // some other initialising
            // this.build_googlechart();
        },
        build_googlechart: function (chart)
        {
            this.init_chart();
            
            
            
            google.visualization.events.addListener(_chart, 'error', function (googleError) {
                console.log('test');
                google.visualization.errors.removeError(googleError.id); 
            });
            
            if (chart == "Table") {
                $('.zoom-bar-title').show();
            } else {
                _chart.setChartType("Table");
                _chart.setOptions(this.getOptions("Table"));
                _chart.draw();

                $('#filter_div').hide();
                $('.zoom-bar-title').hide();
            }
            
            this.setMinAndMaxTimeForOneRow();

            // also tried this - with the same result.
            // google.load('visualization', '1.0', {'packages':['corechart'], 'callback': this.drawChart});
        },
        init_chart: function ()
        {
            // var data = new google.visualization.DataTable(); 
            _data = new google.visualization.DataTable(_tableData);
            _original_data = _data;

            // Create a dashboard.
            _dashboard = new google.visualization.Dashboard(document.getElementById('dashboard_div'));

            if (_graphType === 'LineChart') {
                this.lineChart();
            } else {
                this.columnChart();
            }

            this.addChartSwitchListener();
            this.activeExportToCSV();
            // column toggle
            this.activateColumnToggle();
            this.updateColumnDropDown();
            this.activateColumnsDropDownMenu();
            this.setColumnsIfAny();
            // row toggle
            this.activateRowToggle();
            this.updateRowDropDown();
            this.activateRowsDropDownMenu();
            this.setRowsIfAny();

            $("body").off("click", ".save_chart2");
            $('body').on('click', '.save_chart2', function (e) {
                var image = new Image();
                image.src = _chart.getChart().getImageURI();

                var w = window.open("");
                w.document.write(image.outerHTML);

                e.preventDefault();
            });

        },
        getImageURI: function () {
            return _chart.getChart().getImageURI();
        },
        encodeImage: function () {
            return _chart.getChart().getImageURI();
        },
        setRowsIfAny: function () {
            if (_initiallySelectedRows !== null) {
                this.switchOnAllRows();
                // use original data
                //_view = new google.visualization.DataView(_data);
                // set columns to display  
                //_view.setColumns([1,2,3]);
                //  _dashboard.draw(_view);

                $.each(_rows, function (index, item) {
                    if ($.inArray($.trim(_rows[index]['label']), _initiallySelectedRows) != -1) {
                        // item was found in the arrray  
                        this.performRowToggle(parseInt(index)); // unset
                        this.performRowToggle(parseInt(index)); // set
                    } else {
                        this.performRowToggle(parseInt(index)); // unset

                        var $inp = $('.list_of_rows a input[data-row-id=' + index + ']');
                        $inp.prop('checked', false);
                        $inp.removeAttr('checked');
                    }


                }.bind(this));

                if (_toggledRows.length < 1) {
                    this.switchOnAllRows();
                }
            }
        },
        getSelectedRows: function () {
            if (typeof _rows === "undefined") {
                return false;
            }

            var array = [];

            $.each(_rows, function (index, item) {
                if (_rows[index]['status'] === 1) {
                    array.push($.trim(_rows[index]['label']));
                }
            });

            return array;
        },
        setSelectedRows: function (rows)
        {

            _initiallySelectedRows = rows;
        },
        setColumnsIfAny: function () {
            if (_initialySelectedColumns !== null) {
                this.switchOnAllColumns();
                // use original data
                //_view = new google.visualization.DataView(_data);
                // set columns to display  
                //_view.setColumns([1,2,3]);
                //  _dashboard.draw(_view);

                $.each(_columns, function (index, item) {

                    if (index != 0) {

                        if ($.inArray($.trim(_columns[index]['label']), _initialySelectedColumns) != -1) {
                            // item was found in the arrray  
                            this.performColumnToggle(parseInt(index)); // unset
                            this.performColumnToggle(parseInt(index)); // set
                        } else {
                            this.performColumnToggle(parseInt(index)); // unset

                            var $inp = $('.list_of_columns a input[data-column-id=' + index + ']');

                            $inp.prop('checked', false)

                            $inp.removeAttr('checked');
                        }

                    }


                }.bind(this));

                if (_toggledColumns.length < 2) {
                    this.switchOnAllColumns();
                }
            }
        },
        getSelectedColumns: function () {
            if (typeof _columns === "undefined") {
                return false;
            }

            var array = [];

            $.each(_columns, function (index, item) {
                if (_columns[index]['status'] === 1) {
                    array.push($.trim(_columns[index]['label']));
                }
            });

            return array;
        },
        setSelectedColumns: function (columns)
        {
            _initialySelectedColumns = columns;
        },
        openEditor: function ()
        {
            _chartEditor = new google.visualization.ChartEditor();
            google.visualization.events.addListener(_chartEditor, 'ok', this.redrawChart.bind(this));
            _chartEditor.openDialog(_chart, {});
        },
        redrawChart: function ()
        {
            _chartEditor.getChartWrapper().draw(document.getElementById('chart_div'), this.getOptions('Chart'));
        },
        activateRowsDropDownMenu: function ()
        {
            $("body").off("click", ".list_of_rows .toggleRows");
            $('body').on('click', '.list_of_rows .toggleRows', function (event) {

                event.preventDefault();
                // toggle all columns
                if ($(event.currentTarget).hasClass('checked')) {
                    this.switchOffAllRows();
                    $(event.currentTarget).removeClass('checked')
                } else {
                    this.switchOnAllRows();
                    $(event.currentTarget).addClass('checked')
                }
                $(event.target).blur();


                return false;
            }.bind(this));

            _selectedRows = [];
            $("body").off("click", ".list_of_rows a input");
            $('body').on('click', '.list_of_rows a input', function (event) {
                var $target = $(event.currentTarget),
                        $inp = $target,
                        val = $inp.attr('data-row-id'),
                        idx;
                //if ((idx = _selectedColumns.indexOf(val)) > -1 || $inp.attr('checked')) {
                //    _selectedColumns.splice(idx, 1);

                if (_selectedRows.length === 1 && _rows[val]['status'] === 1) {
                    return false;
                } else if (_rows[val]['status'] === 1) {
                    setTimeout(function () {
                        $inp.prop('checked', false)
                    }, 0);
                    $inp.removeAttr('checked');
                } else {
                    //_selectedColumns.push(val);

                    setTimeout(function () {
                        $inp.prop('checked', true)
                    }, 0);
                }
                this.performRowToggle(parseInt(val));

                $(event.target).blur();


                return false;
            }.bind(this));

            //$('.list_of_columns a').addEventListener('click', function (event) {
            $('body').on('click', '.list_of_columns a', function (event) {
                return false;
            }.bind(this));
        },
        activateColumnsDropDownMenu: function ()
        {
            $("body").off("click", ".list_of_columns .toggleColumns");
            $('body').on('click', '.list_of_columns .toggleColumns', function (event) {

                event.preventDefault();
                // toggle all columns
                if ($(event.currentTarget).hasClass('checked')) {
                    this.switchOffAllColumns();
                    $(event.currentTarget).removeClass('checked')
                } else {
                    this.switchOnAllColumns();
                    $(event.currentTarget).addClass('checked')
                }
                $(event.target).blur();


                return false;
            }.bind(this));

            _selectedColumns = [];
            $("body").off("click", ".list_of_columns a input");
            $('body').on('click', '.list_of_columns a input', function (event) {
                var $target = $(event.currentTarget),
                        $inp = $target,
                        val = $inp.attr('data-column-id'),
                        idx;
                //if ((idx = _selectedColumns.indexOf(val)) > -1 || $inp.attr('checked')) {
                //    _selectedColumns.splice(idx, 1);

                if (_toggledColumns.length === 2 && _columns[val]['status'] === 1) { // even if we have 10 columns we must not forget to include initial column, in our case it would be time, due to which rather then 1 we use 2
                    return false;
                } else if (_columns[val]['status'] === 1) {
                    setTimeout(function () {
                        $inp.prop('checked', false)
                    }, 0);
                    $inp.removeAttr('checked');
                } else {
                    //_selectedColumns.push(val);

                    setTimeout(function () {
                        $inp.prop('checked', true)
                    }, 0);
                }
                this.performColumnToggle(parseInt(val));

                $(event.target).blur();


                return false;

            }.bind(this));


            //$('.list_of_columns a').addEventListener('click', function (event) {
            $('body').on('click', '.list_of_columns a', function (event) {
                return false;
            }.bind(this));
        },
        addChartSwitchListener: function ()
        {
            _showChart = "Chart";
            // activate chart toggle

            //$( "body" ).off( "click", "#btnSwitch");
            //$('body').on('click', '#btnSwitch', this.switchChart.bind(this));

            //_button = document.getElementById('btnSwitch');
            //_button.removeEventListener("click", this.switchChart.bind(this), false);
            //_button.addEventListener('click', this.switchChart.bind(this), false);

            $("body").off("click", "#btnSwitch");
            $('body').on('click', '#btnSwitch', this.switchChart.bind(this));

        },
        swapChart: function ()
        {
            _showChart = $('#btnSwitch').attr('data-name');
            // by default we are switching to see Table
            var chart = "Table";
            if (_showChart === "Chart") {
                chart = _graphType;
            }

            _chart.setChartType(chart);
            _chart.setOptions(this.getOptions(_showChart));


            // update button's value
            _showChart = (_showChart === 'Table') ? 'Chart' : 'Table';
            $('#btnSwitch').val(Lang.get('dataviewer.' + _showChart));

            $('#btnSwitch').attr('data-name', _showChart);

            if (chart === "Table") {

                $('.save_chart').hide();
                $('#filter_div').hide();
                $('.zoom-bar-title').hide();
                $('.report-btn-group').hide();
            } else {

                if (_graphType === 'LineChart') {
                    $('#filter_div').show();
                    $('.zoom-bar-title').show();
                }

                $('.report-btn-group').show();

            }

            _chart.draw();

            if (chart !== "Table") {
                $('.save_chart').show();
                $('.save_chart').removeClass('disabled');
                $('.save_chart').attr('href', _chart.getChart().getImageURI());
            }

            this.resetZoom();
        },
        switchChart: function ()
        {
            excludeflag = true;


            // perform switch
            this.swapChart();
        },
        getOptions: function (chartType)
        {
            var options;

            _graphType
            switch (chartType) {
                case 'Chart':

                    if (_graphType == "LineChart") {
                        options = {
                            width: '100%',
                            height: '100%',
                            'interpolateNulls': true,
                            backgroundColor: {
                                fill: 'transparent'
                            },
                            legend: 'right',
                            pointSize: 0,
                            crosshair: {
                                trigger: 'both'
                            },
                            hAxis: {
                                title: _hAxisTitle,
                            },
                            vAxis: {
                                title: _vAxisTitle,
                            },
                            explorer: {
                                keepInBounds: true,
                                axis: 'horizontal'
                            }
                        };
                    } else {

                        options = {
                            width: '100%',
                            height: '100%',
                            'interpolateNulls': true,
                            backgroundColor: {fill: 'transparent'},
                            'legend': 'none',
                            pointSize: 0,
                            crosshair: {trigger: 'both'}, // Display crosshairs on focus and selection.
                            hAxis: {
                                title: _hAxisTitle,
                            },
                            vAxis: {
                                title: _vAxisTitle,
                            },
                            explorer: {
                                keepInBounds: true,
                                axis: 'horizontal'
                            },
                        };
                    }
                    break;
                case 'Table':
                    options = {
                        'interpolateNulls': true,
                        showRowNumber: true,
                        frozenColumns: 1,
                        //width: '100%',
                        height: '100%',
                        allowHtml: true,
                    };
                    break;
                default:
                    options = {};
            }

            return options;
        },
        lineChart: function ()
        {
            _lineChartRangeFilter = new google.visualization.ControlWrapper({
                'controlType': 'ChartRangeFilter',
                'containerId': 'filter_div',
                'options': {
                    'interpolateNulls': true,
                    filterColumnIndex: 0,
                    ui: {
                        chartType: 'LineChart',
                        chartOptions: {
                            'interpolateNulls': true,
                            backgroundColor: {fill: 'transparent'},
                            height: '50',
                            chartArea: {
                                width: '90%'
                            }
                        }
                    }
                }
            });

            var options = {
                'interpolateNulls': true,
                'tooltip': {trigger: 'selection'},
                backgroundColor: {fill: 'transparent'},
                'legend': 'right',
                pointSize: 0,
                crosshair: {trigger: 'both'}, // Display crosshairs on focus and selection. 
                hAxis: {
                    //viewWindowMode: 'pretty',
                    title: _hAxisTitle,
//                        viewWindow: {
//                            min: _minTime,
//                            max: _maxTime,
//                        },
                },
                vAxis: {
                    title: _vAxisTitle,
                },
                explorer: {
                    keepInBounds: true,
                    axis: 'horizontal'
                },
                selectionMode: 'multiple',
            };

            _chart = new google.visualization.ChartWrapper({
                'chartType': "LineChart",
                'containerId': 'chart_div',
                'options': options
            });
            




            // had to use override below to fix issue with graph not being loaded on initial load

            ///_chart.setOptions(this.getOptions('Chart'));
// Establish dependencies, declaring that 'filter' drives 'pieChart',
            // so that the pie chart will only display entries that are let through
            // given the chosen slider range.
            _dashboard.bind(_lineChartRangeFilter, _chart);
            // Draw the dashboard. 
            _dashboard.draw(_tableData);


            excludeflag = true;



            google.visualization.events.addListener(_chart, 'ready', function () {

                if (_chart.getChartType() === 'LineChart') {
                    _chart.getChart().setAction({
                        id: 'use_for_daily_calc',
                        text: 'Use for Daily Calc',
                        visible: function () {


                            if (typeof (_chart.getChart().getSelection) == 'undefined')
                                return false;
                            if (typeof (_chart.getChart().getSelection()[0]) == 'undefined')
                                return false;
                            if (_chart.getChart().getSelection().length > 2) {
                                return false;
                            }
                            var tmpData = _chart.getDataTable();
                            var selection = _chart.getChart().getSelection()[0];
                            var param = tmpData.getColumnId(selection.column);


                            if (param.trim() == "Leq [dB]" || param.trim() == "1_Leq [dB]" || param.trim() == "2_Leq [dB]"
                                    || param.trim() == "1S_Leq [dB]" || param.trim() == "1L_Leq [dB]"
                                    || param.trim() == "2S_Leq [dB]" || param.trim() == "2L_Leq [dB]") {
                                return true;
                            }

                            return false;
                        },
                        action: function () {
                            var tmpData = _chart.getDataTable();
                            var selection = _chart.getChart().getSelection()[0];
                            var param = tmpData.getColumnId(selection.column);
                            var value = tmpData.getValue(selection.row, selection.column);
                            var time = Math.round((new Date(tmpData.getFormattedValue(selection.row, 0))).getTime() / 1000);

                            window.location.replace("/user/noise_at_work/exposure_calculator/daily?preLoad=" + fileId + "&leq=" + value);

                            // var linkurl = "/data?param=" + param + "&value=" + value + "&time=" + time; 
                            // data.setCell(_chart.getChart().getSelection()[0].row, 1, data.getValue(_chart.getSelection()[0].row, 1) + 20);
                            // _chart.draw(data, options);
                        }
                    });

                    _chart.getChart().setAction({
                        id: 'use_for_hpd',
                        text: 'Use for HPD',
                        visible: function () {
                            if (typeof (_chart.getChart().getSelection) == 'undefined')
                                return false;
                            if (typeof (_chart.getChart().getSelection()[0]) == 'undefined')
                                return false;

                            if (_chart.getChart().getSelection().length > 2) {
                                return false;
                            }
                            var tmpData = _chart.getDataTable();
                            var selection = _chart.getChart().getSelection()[0];
                            var param = tmpData.getColumnId(selection.column);

                            if (param.trim() == "Leq [dB]" || param.trim() == "1_Leq [dB]" || param.trim() == "2_Leq [dB]"
                                    || param.trim() == "1S_Leq [dB]" || param.trim() == "1L_Leq [dB]"
                                    || param.trim() == "2S_Leq [dB]" || param.trim() == "2L_Leq [dB]") {
                                return true;
                            }

                            return false;
                        },
                        action: function () {
                            var tmpData = _chart.getDataTable();
                            var selection = _chart.getChart().getSelection()[0];
                            var param = tmpData.getColumnId(selection.column);
                            var value = tmpData.getValue(selection.row, selection.column);
                            //var time = Math.round((new Date(tmpData.getFormattedValue(selection.row, 0))).getTime() / 1000);
                            var time = moment(tmpData.getFormattedValue(selection.row, 0));
                            var interval = $('#settingsInterval').val().toLowerCase();
                            if (interval === "mergeintervals") {
                                if (param.trim().slice(1, 1) === "S") {
                                    interval = "short";
                                } else {
                                    interval = "long";
                                }
                            }

                            window.location.replace("/user/hpd?file_id=" + fileId + "&time=" + moment(tmpData.getValue(selection.row, 0)).format('YYYY-MM-DD HH:mm:ss') + "&ms=" + moment(tmpData.getValue(selection.row, 0)).format('x') + "&interval=" + interval);

                            // var linkurl = "/data?param=" + param + "&value=" + value + "&time=" + time; 
                            // data.setCell(_chart.getChart().getSelection()[0].row, 1, data.getValue(_chart.getSelection()[0].row, 1) + 20);
                            // _chart.draw(data, options);
                        }
                    });


                    $('.save_chart').attr('href', _chart.getChart().getImageURI());

                } else {
                    if (excludeflag) {
                        excludeflag = false;
                        var tmpData = _chart.getDataTable();
                        var table = document.getElementById('chart_div').getElementsByTagName('div')[0].getElementsByTagName('div')[0].getElementsByTagName('table')[0].getElementsByTagName('tbody')[0];
                        rows = table.rows;

                        if (typeof rows[0] !== "undefined") {
                            for (var j = 0; j < rows[0].cells.length; j++) {
                                var str = rows[0].cells[j].innerHTML;
                                if (str.search("color") != -1 || str == "") {
                                    for (var k = 0; k < rows.length; k++) {
                                        rows[k].deleteCell(j);
                                    }
                                }
                            }
                            table = document.getElementById('chart_div').getElementsByTagName('div')[0].getElementsByTagName('div')[0].getElementsByTagName('table')[0].getElementsByTagName('thead')[0];
                            rows = table.rows;

                            for (var j = 0; j < rows[0].cells.length; j++) {
                                var str = rows[0].cells[j].innerHTML;
                                if (str[0] == "<") {
                                    for (var k = 0; k < rows.length; k++) {
                                        rows[k].deleteCell(j);
                                    }
                                }
                            }
                            for (var i = 0; i < tmpData.getNumberOfRows(); i++) {
                                //var idate = moment(newData.getFormattedValue(i, 0));
                                var idate = new Date(tmpData.getValue(i, 0));
                                //var ival = idate.format('YYYY-MM-DD HH:mm:ss');
                                for (var k = 0; k < excludetime.length; k++) {
                                    if (idate > new Date(excludetime[k].start) && idate <= new Date(excludetime[k].end)) {
                                        document.getElementById('chart_div').getElementsByTagName('div')[0].getElementsByTagName('div')[0].getElementsByTagName('table')[0].getElementsByTagName('tbody')[0].getElementsByTagName('tr')[i].style.backgroundColor = 'grey';
                                        // for (var j = 0; j < tmpData.getNumberOfColumns(); j++) {
                                        //     if (tmpData.getValue(i,j) == "color : black") {
                                        //         document.getElementById('chart_div').getElementsByTagName('div')[0].getElementsByTagName('div')[0].getElementsByTagName('table')[0].getElementsByTagName('tbody')[0].getElementsByTagName('tr')[i].getElementsByTagName('td')[j+1].innerHTML  = "";
                                        //     }
                                        // }
                                    }
                                }
                            }
                        }
                    }
                }
            });

            google.visualization.events.addListener(_chart, 'select', function () {
                var selection = _chart.getChart().getSelection();
                var tmpData = _chart.getDataTable();

                if (selection.length > 2) {
                    _chart.getChart().setSelection([]);
                    return;
                }

                if (!selectionenable && selection.length != 0 && selection[0].column !== null && selection[0].row != null) {
                    if (selection.length > 1) {

                        selection.splice(0, 1);
                        _chart.getChart().setSelection(selection);
                    }

                    var param = tmpData.getColumnId(selection[0].column);
                    var value = tmpData.getValue(selection[0].row, selection[0].column);
                    var time = Math.round((new Date(tmpData.getFormattedValue(selection[0].row, 0))).getTime() / 1000);
                    var linkurl = "/data?param=" + param + "&value=" + value + "&time=" + time;

                    var control = '<li><a href="' + linkurl + '">' + linkurl + '</a></li>';

                    $("#myDropdown").empty();
                    $("#myDropdown").append(control);

                    //$("#clickMenu").show();

                    //$("#clickMenu").parent().css({position: 'relative'});
                    //$("#clickMenu").css({top: mouse.y, left: mouse.x, position:'absolute'});
                } else {
                    //$("#clickMenu").hide();
                    if (selection.length == 1 && selectflag) {
                        selectflag = false;
                        $('.deletebutton').addClass('disabled');
                        var newData = new google.visualization.DataTable();
                        var _data = new google.visualization.DataTable(_tableData);
                        newData.addRows(tmpData.getNumberOfRows());
                        for (var c = 0; c < tmpData.getNumberOfColumns(); c++) {
                            if (c % 2 == 1)
                                newData.addColumn({type: 'string', role: 'style'});
                            else
                                newData.addColumn(tmpData.getColumnType(c), tmpData.getColumnLabel(c), tmpData.getColumnId(c));
                        }
                        for (var i = 0; i < tmpData.getNumberOfRows(); i++) {
                            for (var j = 0; j < tmpData.getNumberOfColumns(); j++) {
                                newData.setCell(i, j, tmpData.getValue(i, j));
                                if (j % 2 == 1) {
                                    newData.setCell(i, j, "");
                                }
                            }
                        }
                        var _view = new google.visualization.DataView(newData);
                        _dashboard.draw(_view);
                    }

                    if (selection.length == 2) {
                        selectflag = true;
                        if (selection[0].row > selection[1].row) {
                            var temp = selection[0];
                            selection[0] = selection[1];
                            selection[1] = temp;
                        }
                        $('.deletebutton').removeClass('disabled');
                        var newData = new google.visualization.DataTable();
                        var _data = new google.visualization.DataTable(_tableData);
                        newData.addRows(tmpData.getNumberOfRows());
                        if (tmpData.getNumberOfColumns() > _data.getNumberOfColumns()) {
                            for (var c = 0; c < tmpData.getNumberOfColumns(); c++) {
                                if (c % 2 == 1)
                                    newData.addColumn({type: 'string', role: 'style'});
                                else
                                    newData.addColumn(tmpData.getColumnType(c), tmpData.getColumnLabel(c), tmpData.getColumnId(c));
                            }
                            for (var i = 0; i < tmpData.getNumberOfRows(); i++) {
                                for (var j = 0; j < tmpData.getNumberOfColumns(); j++) {
                                    newData.setCell(i, j, tmpData.getValue(i, j));
                                    if (i > selection[0].row && i <= selection[1].row && j % 2 == 1) {
                                        newData.setCell(i, j, "color : grey");
                                    }
                                }
                            }
                        } else {
                            for (var c = 0; c < tmpData.getNumberOfColumns(); c++) {
                                newData.addColumn(tmpData.getColumnType(c), tmpData.getColumnLabel(c), tmpData.getColumnId(c));
                                newData.addColumn({type: 'string', role: 'style'});
                            }
                            for (var i = 0; i < tmpData.getNumberOfRows(); i++) {
                                for (var j = 0; j < tmpData.getNumberOfColumns(); j++) {
                                    newData.setCell(i, 2 * j, tmpData.getValue(i, j));
                                    newData.setCell(i, 2 * j + 1, "");
                                    if (i > selection[0].row && i <= selection[1].row) {
                                        newData.setCell(i, 2 * j + 1, "color : grey");
                                    }
                                }
                            }
                        }
                        var _view = new google.visualization.DataView(newData);
                        _dashboard.draw(_view);
                        startpoint = selection[0].row;
                        endpoint = selection[1].row;
                    }
                }
            });



        },

        removeSeleted: function () {
            if (!selectflag)
                return;

            //$("#clickMenu").hide();
            selectflag = false;
            $('.deletebutton').addClass('disabled');
            var newData = new google.visualization.DataTable();
            var tmpData = _chart.getDataTable();
            newData.addRows(tmpData.getNumberOfRows());
            for (var c = 0; c < tmpData.getNumberOfColumns(); c++) {
                if (c % 2 == 1)
                    newData.addColumn({type: 'string', role: 'style'});
                else
                    newData.addColumn(tmpData.getColumnType(c), tmpData.getColumnLabel(c), tmpData.getColumnId(c));
            }
            for (var i = 0; i < tmpData.getNumberOfRows(); i++) {
                for (var j = 0; j < tmpData.getNumberOfColumns(); j++) {
                    newData.setCell(i, j, tmpData.getValue(i, j));
                    if (i > startpoint && i <= endpoint && j % 2 == 1) {
                        newData.setCell(i, j, "color : black");
                    }
                }
            }
            var _view = new google.visualization.DataView(newData);
            _dashboard.draw(_view);


            var stDateTime = moment(newData.getFormattedValue(startpoint, 0));
            var etDateTime = moment(newData.getFormattedValue(endpoint, 0));

            //var stDateTime = (newData.getValue(startpoint, 0));
            //var etDateTime = (newData.getValue(endpoint, 0));

//            var st = String(stDateTime.getYear() + "-" + stDateTime.getMonth() + "0" + stDateTime.getDay()) + " " + String(stDateTime.getHours() + ":" + stDateTime.getMinutes() + ":" + stDateTime.getSeconds());
            //           var et = String(etDateTime.getYear() + "-" + etDateTime.getMonth() + "0" + etDateTime.getDay()) + " " + String(etDateTime.getHours() + ":" + etDateTime.getMinutes() + ":" + etDateTime.getSeconds());
            //var st = stDateTime.format('YYYY-MM-DD HH:mm:ss');
            //var et = etDateTime.format('YYYY-MM-DD HH:mm:ss');

            var stOld = Math.floor((new Date(newData.getFormattedValue(startpoint, 0))).getTime() / 1000);
            var etOld = Math.floor((new Date(newData.getFormattedValue(endpoint, 0))).getTime() / 1000);
            addRestoreItem(String(stOld), String(etOld), String(stDateTime.format('HH:mm:ss')), String(etDateTime.format('HH:mm:ss')));
            //addRestoreItem(String(stOld), String(etOld), String(stDateTime), String(etDateTime));
            excludetime.push({start: stDateTime, end: etDateTime});

            getJsonData();
        },

        updateData: function (data) {

            // var data = new google.visualization.DataTable(); 
            _tableData = data;
            _data = new google.visualization.DataTable(_tableData);
            _original_data = _data;
            _initialySelectedColumns = _toggledColumns;

            this.lineChart();


            _view = new google.visualization.DataView(_original_data);



            // set columns to display
            _view.setColumns(_toggledColumns);

            _dashboard.draw(_view);

            return true;
        },
        columnChart: function () {
            _chart = new google.visualization.ChartWrapper({
                'chartType': "ColumnChart",
                'containerId': 'chart_div',
                'dataTable': _data,
                'options': {
                    width: '100%',
                    height: '100%',
                    backgroundColor: {fill: 'transparent'},
                    'legend': 'none',
                    pointSize: 0,
                    crosshair: {trigger: 'both'}, // Display crosshairs on focus and selection.
                    hAxis: {
                        title: _hAxisTitle,
                    },
                    vAxis: {
                        title: _vAxisTitle,
                    },
                    explorer: {
                        keepInBounds: true,
                        axis: 'horizontal'
                    },
                }
            });


            $('.zoom-bar-title').hide();

        },
        dataTableToCSV: function () {
            var dt_cols = _data.getNumberOfColumns();
            var dt_rows = _data.getNumberOfRows();

            var csv_cols = [];
            var csv_out;

            // Iterate columns
            for (var i = 0; i < dt_cols; i++) {
                // Replace any commas in column labels
                // push if column is active
                if (_columns[i]['status'] === 1) {
                    csv_cols.push(_data.getColumnLabel(i).replace(/,/g, ""));
                }
            }

            // Create column row of CSV
            csv_out = csv_cols.join(",") + "\r\n";

            // Iterate rows
            for (i = 0; i < dt_rows; i++) {
                var raw_col = [];

                if (_rows[i]['status'] === 0) {
                    continue;
                }

                for (var j = 0; j < dt_cols; j++) {
                    // Replace any commas in row values

                    // push if column is active
                    if (_columns[j]['status'] === 1) {
                        raw_col.push(_data.getFormattedValue(i, j).replace(/,/g, ""));
                    }
                }
                // Add row to CSV text
                csv_out += raw_col.join(",") + "\r\n";
            }

            _csv_out = csv_out;
        },
        downloadCSV: function ()
        {
            var blob = new Blob([_csv_out], {type: 'text/csv;charset=utf-8'});
            var url = window.URL || window.webkitURL;
            var link = document.createElementNS("http://www.w3.org/1999/xhtml", "a");
            link.href = url.createObjectURL(blob);
            link.download = "export.csv";

            var event = document.createEvent("MouseEvents");
            event.initEvent("click", true, false);
            link.dispatchEvent(event);
        },
        activeExportToCSV: function ()
        {
            $('body').on('click', '.export_to_csv', function () {
                // get csv output
                var csv = this.dataTableToCSV();

                // start download 
                this.downloadCSV(csv, 'test');
            }.bind(this));
        },
        toggleColumnsOnReady: function ()
        {

        },
        activateColumnToggle: function ()
        {
            _columns = {}; // list of all columns and their labels + status
            _toggledColumns = []; // list of active columns
            _columnNamesWithId = [];
            //_series = {};
            for (var i = 0; i < _original_data.getNumberOfColumns(); i++) {
                _columns[i] = {
                    label: _original_data.getColumnLabel(i),
                    //status: (_initialySelectedColumns !== null ? 0 : 1)
                    status: 1
                };

                //if (_initialySelectedColumns === null || $.inArray($.trim(_original_data.getColumnLabel(i)), _initialySelectedColumns) !== -1) {
                _toggledColumns.push(i);
                //}

                _columnNamesWithId[$.trim(_original_data.getColumnLabel(i))] = i;

                //if (i > 0) {
                //    _series[i - 1] = {};
                //}
            }

            //console.log(_columns);

            //google.visualization.events.addListener(_lineChart, 'select', this.columnToggle.bind(this));
        },
        updateColumnDropDown: function () {
            $('.list_of_columns ul').html("");
            for (var i = 1; i < Object.keys(_columns).length; i++) {
                $('.list_of_columns ul').append('<li>' +
                        '<a href="#" class="small" tabIndex="-1">' +
                        '<input type="checkbox"  name="columns[' + _columns[i]['label'].trim() + ']" data-column-id="' + i + '" checked' +
                        ' />&nbsp;' + _columns[i]['label'] +
                        '</a>' +
                        '</li>');
            }

            $('.list_of_columns ul').prepend('<li>' +
                    '<a href="#" class="small toggleColumns checked">' +
                    '&nbsp; ' + Lang.get('dataviewer.select_diselect_all') +
                    '</a>' +
                    '</li>');
        },
        activateRowToggle: function ()
        {
            _rows = {}; // list of all columns and their labels + status
            _toggledRows = []; // list of active columns

            //_series = {};
            for (var i = 0; i < _original_data.getNumberOfRows(); i++) {
                _rows[i] = {
                    label: _original_data.getValue(i, 0),
                    status: 1
                };

                _toggledRows.push(i);

                //if (i > 0) {
                //    _series[i - 1] = {};
                //}
            }

            //google.visualization.events.addListener(_lineChart, 'select', this.columnToggle.bind(this));
        },
        updateRowDropDown: function () {
            $('.list_of_rows ul').html("");

            for (var i = 0; i < Object.keys(_rows).length; i++) {
                $('.list_of_rows ul').append('<li>' +
                        '<a href="#" class="small">' +
                        '<input type="checkbox"  name="rows[' + _rows[i]['label'] + ']" data-row-id="' + i + '" ' + (_rows[i]['status'] === 1 ? 'checked' : '') + ' />&nbsp;' + _rows[i]['label'] +
                        '</a>' +
                        '</li>');
            }


            $('.list_of_rows ul').prepend('<li>' +
                    '<a href="#" class="small toggleRows checked" >' +
                    '&nbsp; ' + Lang.get('dataviewer.select_diselect_all') +
                    '</a>' +
                    '</li>');
        },
        switchOffAllRows: function () {
            // use original data
            _view = new google.visualization.DataView(_original_data);

            for (var row in _rows) {
                var rowNumber = parseInt(row);

                var key = $.inArray(row, _toggledRows);
                if (_rows[rowNumber]['status'] === 1) {
                    _rows[rowNumber]['status'] = 0;
                    // insert new items with a key and push other columns
                    _toggledRows.splice(key, 1);
                }
            }


            // set columns to display
            _view.setRows(_toggledRows);
            _chart.setView(_view.toJSON());
            _chart.draw();

            // check all the checkboxes 
            $('.list_of_rows ul input').each(function () {
                $(this).prop('checked', false);
            });
        },
        switchOnAllRows: function () {
            // use original data
            _view = new google.visualization.DataView(_original_data);

            for (var row in _rows) {
                var rowNumber = parseInt(row);



                if (_rows[rowNumber]['status'] === 0) {
                    _rows[rowNumber]['status'] = 1;
                    // insert new items with a key and push other columns
                    _toggledRows.splice(rowNumber, 0, rowNumber);
                }
            }


            // set columns to display
            _view.setRows(_toggledRows);
            _chart.setView(_view.toJSON());
            _chart.draw();

            // check all the checkboxes 
            $('.list_of_rows ul input').each(function () {
                $(this).prop('checked', true);
            });
        },
        performRowToggle: function (row)
        {
            // use original data
            _view = new google.visualization.DataView(_original_data);

            // get a key due to splice not keeping keys one delete
            var key = $.inArray(row, _toggledRows);

            if (_rows[row]['status'] === 1) {
                _rows[row]['status'] = 0;
                // delete by key because splice doesn't keep keys on delete

                _toggledRows.splice(key, 1);

            } else {
                _rows[row]['status'] = 1;
                // insert new items with a key and push other columns
                _toggledRows.splice(row, 0, row);
            }

            // set columns to display
            _view.setRows(_toggledRows);
            _chart.setView(_view.toJSON());
            _chart.draw();
        },
        switchOffAllColumns: function () {
            // use original data
            _view = new google.visualization.DataView(_original_data);

            for (var col in _columns) {
                var columnNumber = parseInt(col);

                if (columnNumber === 0 || columnNumber === 1) {
                    if (columnNumber === 1) {
                        if (_columns[columnNumber]['status'] === 0) {
                            _columns[columnNumber]['status'] = 1;
                            // insert new items with a key and push other columns
                            _toggledColumns.splice(columnNumber, 0, columnNumber);
                        }
                    }

                    continue;
                }

                var key = $.inArray(col, _toggledColumns);
                if (_columns[columnNumber]['status'] === 1) {
                    _columns[columnNumber]['status'] = 0;
                    // insert new items with a key and push other columns
                    _toggledColumns.splice(key, 1);
                }
            }
            _chart.setView({
                columns: _toggledColumns
            });

            _dashboard.draw(_view);

            // check all the checkboxes
            var count = 0;
            $('.list_of_columns ul input').each(function () {
                if (count == 0) {
                    $(this).prop('checked', true);
                    count++;
                    return;
                }

                $(this).prop('checked', false);
                count++;
            });
        },
        switchOnAllColumns: function () {
            // use original data
            _view = new google.visualization.DataView(_original_data);

            for (var col in _columns) {
                var columnNumber = parseInt(col);

                if (columnNumber === 0) {
                    continue;
                }

                if (_columns[columnNumber]['status'] === 0) {
                    _columns[columnNumber]['status'] = 1;
                    // insert new items with a key and push other columns
                    _toggledColumns.splice(columnNumber, 0, columnNumber);
                }
            }

            // set columns to display
            _chart.setView({
                columns: _toggledColumns
            });

            _dashboard.draw(_view);

            // check all the checkboxes
            $('.list_of_columns ul input').each(function () {
                $(this).prop('checked', true);
            });
        },
        performColumnToggle: function (col)
        {
            // use original data
            _view = new google.visualization.DataView(_original_data);

            // get a key due to splice not keeping keys one delete
            var key = $.inArray(col, _toggledColumns);

            if (_columns[col]['status'] === 1) {
                _columns[col]['status'] = 0;
                // delete by key because splice doesn't keep keys on delete 
                _toggledColumns.splice(key, 1);

            } else {
                _columns[col]['status'] = 1;
                // insert new items with a key and push other columns
                _toggledColumns.splice(col, 0, col);
            }

            // set columns to display

            _chart.setView({
                columns: _toggledColumns
            });

            //_view.hideColumns(_toggledColumns);
            //_view.setColumns(_toggledColumns);
            //console.log(_view.getNumberOfRows()); 
            _dashboard.draw(_view);


        },
        resetZoom: function () {

            if (_graphType == "LineChart") {
                var tmpData = _chart.getDataTable();
                var _view = new google.visualization.DataView(tmpData);
                _dashboard.draw(_view);


            } else if (_graphType == "ColumnChart") {
                _chart.draw();
            }
        },
        /**
         * Method used to apply min/max time fix for when line chart has one row of data
         * 
         * @returns {undefined}
         */
        setMinAndMaxTimeForOneRow: function () {
            var tmpData = _data;
            // can only be used with line chart and data must be present
            // can only be used with one row
            if (_graphType != "LineChart" || tmpData === null || tmpData.getNumberOfRows() != 1) {
                return;
            }

            var dateRange = tmpData.getColumnRange(0);

            var options = this.getOptions(_showChart);
            //options.vAxis.ticks = tmpData.getDistinctValues(0);
            options.hAxis.viewWindow = {
                min: new Date(dateRange.min.getFullYear(), dateRange.min.getMonth(), dateRange.min.getDate(), dateRange.min.getHours(), dateRange.min.getMinutes(), dateRange.min.getSeconds() - 1),
                max: new Date(dateRange.min.getFullYear(), dateRange.min.getMonth(), dateRange.min.getDate(), dateRange.min.getHours(), dateRange.min.getMinutes(), dateRange.min.getSeconds() + 1)
            };

            _chart.setOptions(options);
            _chart.draw();
        }

    };
}());